package GoTasks

import (
    "os"
    "os/exec"
    "path/filepath"
    "io/ioutil"
    "net/url"
    "net/http"
    "strconv"
    "git.oschina.net/janpoem/go-logger"
)

func Exist(path string) bool {
    _, err := os.Stat(path)
    return err == nil || os.IsExist(err)
}

func GetRuntimeDir() string {
    file, _ := exec.LookPath(os.Args[0])
    return filepath.Dir(file)
}

func ReadFileByte(path string) ([]byte, error) {
    fi, err := os.Open(path)
    if err != nil {
        //		panic(err)
        return nil, err
    } else {
        defer fi.Close()
        return ioutil.ReadAll(fi)
    }
}

func ReadFileStr(path string) (string, error) {
    raw, err := ReadFileByte(path)
    return string(raw), err
}

///////////////////////////////////////////////////
// Http方法
///////////////////////////////////////////////////

func HttpGet(url string) (string, error) {
    resp, err := http.Get(url)
    if err != nil {
        logger.Debug("HttpGet", "请求错误:", err)
        return "", err
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        logger.Debug("HttpGet", "IO/Read错误:", err)
        return "", err
    }
    return string(body), err
}

func HttpPost(reqUrl string, post string) (string, error) {
    resp, err := http.PostForm(reqUrl, url.Values{"data": {post}})
    if err != nil {
        logger.Debug("HttpPost", "请求错误:", err)
        return "", err
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        logger.Debug("HttpPost", "IO/Read错误:", err)
        return "", err
    }
    return string(body), err
}

///////////////////////////////////////////////////
// Json解析部分，目前暂时用不上
///////////////////////////////////////////////////

// Value结构
type JsonValue struct {
    Value   interface{}
}

type JsonString struct {
    Value   string
}

type JsonNumber struct {
    Value   float64
}

//func (item *JsonValue) TypeErase()

// 尝试将JsonValue的值转换为一个字符串
func (item *JsonValue) AsString() string {
    v := item.Value
    switch v.(type) {
        case nil :
        return ""
        case string :
        if val, ok := v.(string); ok {
            return val
        }
        case bool :
        if v == true {
            return "true"
        } else {
            return "false"
        }
        // 实际上是没有整型值
        // case int :
        case float64 :
        if val, ok := item.Value.(float64); ok {
            return strconv.FormatFloat(val, 'f', -1, 64)
        }
        case []interface{} :
        // 数组暂时就不做特别处理了
        default:
        // 其它未知数据，返回空值
    }
    return ""
}

// 尝试将JsonValue转换为一个数字类型
func (item *JsonValue) AsNumber() float64 {
    v := item.Value
    switch v.(type) {
        case nil :
        return 0
        case string :
        if val, ok := v.(string); ok {
            float, err := strconv.ParseFloat(val, 64)
            if err == nil {
                return float
            }
        }
        case bool :
        if v == true {
            return 1
        } else {
            return 0
        }
        // 实际上是没有整型值
        // case int :
        case float64 :
        if val, ok := item.Value.(float64); ok {
            return val
        }
        case []interface{} :
        // 数组暂时就不做特别处理了
        default :
        // 其它未知数据，返回空值
    }
    return 0
}

// 检查值是否为null
func (item *JsonValue) IsDefined() bool {
    v := item.Value
    switch v.(type) {
        case nil :
        return true
    }
    return false
}